package com.company;
import jdk.dynalink.Operation;
import java.util.*;
import java.lang.Math.*;
public class Main
{
    public static void main(String[] args)
    {
        //Первое число(number)- выбор уравнений из массивов
        Scanner in=new Scanner(System.in);
        Operationable[]operations=new Operationable[]{x -> 2 * Math.sin(x) + 1, x -> (x / Math.PI - 1) * (x / Math.PI - 1), x -> -(x / Math.PI) * (x / Math.PI) - 2 * x + 5 * Math.PI, x -> Math.cos(x) * Math.cos(x) / 2 + 1};
        Tabulation(operations);
        //    RandomMin(in.nextInt(),in.nextInt(),operations);
        //   RandomMax(in.nextInt(),in.nextInt(),operations);
        Operationable[]operations2=new Operationable[]{x -> x*Math.sin(x)-0.5,x -> Math.log(x*x-3*x+2)/Math.log(10),x ->Math.log(x*x-3*x+2)/Math.log(10),x -> 0.5*Math.tan((2/3*(x+Math.PI/4))-1)};
        //Первое число- номер уравнения из массива. Второе-начало промежутка. Третье-конец промежутка
        BisectionMethod(operations2,in.nextInt(),in.nextDouble(),in.nextDouble());
        Operationable[]operations3=new Operationable[]{x -> 2*Math.sin(x)+1,x -> (-Math.pow((x/Math.PI),2)-2*x+5*Math.PI),x ->(0.5*Math.cos(x)*Math.cos(x)+1)};
        //Первое число- номер уравнения из массива. Второе- количество разбиений
        Pryamougolnik(operations3,in.nextInt(),in.nextInt());
    }
    public static void Tabulation(Operationable[]operation)
    {
        int n=0;
        int k=100;
        for (int i = 0; i < operation.length; i++)
        {
            System.out.println(i+1+" Уравнение:");
            for (double l = -2 * Math.PI; l <= 2 * Math.PI; l += Math.PI / 6)
            {
               double result = operation[i].calculate(l);
                if (result<0)
                    n++;
            }
            System.out.println(n+" отрицательных ответов");
            n=0;
        }

    }
    public static double RandomMin(int number, int n, Operationable[]operation)
    {
        double min=1000;
        for (int i=0; i<n; i++)
        {
            double random=(Math.random()-0.5)*20;
            if (min>operation[number].calculate(random))
                min= operation[number].calculate(random);
        }
        return min;
    }
    public static double RandomMax(int number, int n, Operationable[]operation)
    {
        double max=-1000;
        for (int i=0; i<n; i++)
        {
            double random=(Math.random()-0.5)*20;
            if (max<operation[number].calculate(random))
                max=operation[number].calculate(random);;
        }
        return max;
    }
    public static double BisectionMethod(Operationable[] operation, int number,double a, double b)
    {
        double half=0;
        double dx=0;
        double eps=0.0001;
    if (operation[number].calculate(a)==0)
        return a;
    if (operation[number].calculate(b)==0)
        return b;
    while (b-a>eps)
    {
        dx = (b-a) / 2;
        half = a + dx;
        if (Math.signum(operation[number].calculate(a)) != Math.signum(operation[number].calculate(half)))
            b = half;
        else a = half;
    }return half;
    }
    public static double Pryamougolnik(Operationable[] operation,int number,int n)
    {
        double answer=0;
        double results=0;
        double[]x=new double[n];
        double h = 2*Math.PI / n;
        for (int i = 0; i < n; i++)
        {
            x [i] = -Math.PI + i * h;
            results+= operation[number].calculate(x[i] + h /2) ;
        }
        answer=h*results;
        return answer;
    }
    interface Operationable{
        double calculate(double x);
}
}

package com.company;
import java.io.*;
import java.nio.charset.StandardCharsets;

public class InputData {
    private static String[] k = new String[13];
    private static final String separator =" ";
    private static String fname = "./src/com/company/sad.csv";
    public InputData() {}

    public static void main(String[] args) throws IOException
    {
        int itogo1=0;
        int itogo2=0;
        double itogo3=0;
        ReadCSV();
        System.out.printf("%-3s|%-57s|%-11s|%-39s%n","№","слово "," ","Количество информации");
        System.out.println("---------------------------------------------------------------------------------------------------------------------");
        System.out.printf("%-4s|%-57s|%-11s|%-11s|%14s|%14s%n"," "," "," ", "кол-во","байт, размер",",бит,");
        System.out.printf("%-4s|%-57s|%-11s|%-11s|%14s|%14s%n"," "," ","палиндром", "символов","в программе","по Хартли");
        for(int i=0;i<k.length;i++)
        {
            itogo1+=(k[i].length());
            itogo2+=WordBytes(k[i]);
            itogo3+=HartleyBit(k[i]);
            System.out.println("---------------------------------------------------------------------------------------------------------------------");
            Print(k[i], i + 1);
        }
        System.out.println("---------------------------------------------------------------------------------------------------------------------");
        System.out.printf("%-4s|%-57s|%-11s|%-11d|%14d|%14.2f"," ","ИТОГО "," ",itogo1,itogo2,itogo3);

    }
    public static int WordBytes(String word) throws IOException
    {
        final byte[] utf8Bytes = word.getBytes(StandardCharsets.UTF_8);
        return utf8Bytes.length;
    }
    public static double HartleyBit(String word)
    {
        double alphabet = word.length();
        char[] charword = word.toCharArray();
        for (int i = 0; i < word.length(); i++)
        {
            for (int j = 0; j < word.length(); j++)
            {
                if (charword[i] == charword[j])
                {
                    if (i != j)
                    {
                        if (j > i) {
                            continue;
                        }
                        alphabet -= 1;
                        charword[j] = 0;
                    }
                }
            }
        }
        return word.length() * (Math.log(alphabet) / Math.log(2));
    }

        public static char Palindrom (String word)
        {
            for (int i = 0; i < word.length() / 2; ++i)
            {
                if (word.charAt(i)!= word.charAt(word.length() - i - 1))
                {
                    return '-';
                }
            }
            return '+';
        }

        public static void Print (String print,int i) throws IOException
        {
            System.out.printf("%-4d|%-57s|%-11s|%-11d|%14d|%14.2f%n", i, print, Palindrom(print), print.length(), WordBytes(print), HartleyBit(print));
        }
        public static void ReadCSV ()
        {
            try (BufferedReader br =
                         new BufferedReader(new InputStreamReader(new FileInputStream(fname))))
            {
                String line = "";
                line = br.readLine();
                String[] elements = line.split(separator);
                for (int i = 0; i < elements.length; i++)
                    k[i] = elements[i];
            } catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
            } catch (FileNotFoundException e)
            {
                e.printStackTrace();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

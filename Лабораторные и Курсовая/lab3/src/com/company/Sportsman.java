package com.company;

public class Sportsman
{
    private String name, birthday, sex, place, year, result, award;

    public Sportsman(String name, String birthday, String sex, String place, String year, String result, String award)
    {
        this.name = name;
        this.birthday = birthday;
        this.sex = sex;
        this.place = place;
        this.year = year;
        this.result = result;
        this.award = award;
    }

    public String getName()
    {
        return name;
    }

    public String getBirthday()
    {
        return birthday;
    }

    public void setBirthday(String birthday)
    {
        this.birthday = birthday;
    }

    public String getSex()
    {
        return sex;
    }

    public String getPlace()
    {
        return place;
    }

    public String getYear()
    {
        return place;
    }

    public String getResult()
    {
        return result;
    }

    public String getAward()
    {
        return award;
    }

    @Override
    public String toString()
    {
        return name+birthday+sex+place+year+result+award;
    }
}

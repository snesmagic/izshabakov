package com.company;

public class DateTemp {
    private String day;
    private String month;
    private String temp;
    private String date;

    public DateTemp(String day,String month, String temp) {
        this.day=day;
        this.month=month;
        this.temp = temp;
        this.date = day+"."+month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }
    public String getDate() {
        return date;
    }
}



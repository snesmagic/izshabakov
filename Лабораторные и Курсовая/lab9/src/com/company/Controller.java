package com.company;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;


public class Controller {

    ObservableList<DateTemp> datas = FXCollections.observableArrayList();
    //XYChart.Series series = new XYChart.Series();

    @FXML
    ComboBox Box;
    @FXML
    LineChart<String, Integer> lineChart;
    @FXML
    BarChart<String, Integer> barChart;

    @FXML
    TableView tableView;
    @FXML
    private TableColumn date;
    @FXML
    private TableColumn temp;

    @FXML
    TableView tableViewYear;
    @FXML
    private TableView Jan;
    @FXML
    private TableColumn<DateTemp,String> JanDay;
    @FXML
    private TableColumn<DateTemp,String> JanTemp;
    @FXML
    private TableView Feb;
    @FXML
    private TableColumn<DateTemp,String> FebDay;
    @FXML
    private TableColumn<DateTemp,String> FebTemp;

    @FXML
    private TableView  Mar;
    @FXML
    private TableColumn MarDay;
    @FXML
    private TableColumn MarTemp;

    @FXML
    private TableView Apr;
    @FXML
    private TableColumn AprDay;
    @FXML
    private TableColumn AprTemp;

    @FXML
    private TableView May;
    @FXML
    private TableColumn MayDay;
    @FXML
    private TableColumn MayTemp;

    @FXML
    private TableView Jun;
    @FXML
    private TableColumn JunDay;
    @FXML
    private TableColumn JunTemp;

    @FXML
    private TableView Jul;
    @FXML
    private TableColumn JulDay;
    @FXML
    private TableColumn JulTemp;

    @FXML
    private TableView Aug;
    @FXML
    private TableColumn AugDay;
    @FXML
    private TableColumn AugTemp;

    @FXML
    private TableView Sep;
    @FXML
    private TableColumn SepDay;
    @FXML
    private TableColumn SepTemp;

    @FXML
    private TableView Oct;
    @FXML
    private TableColumn OctDay;
    @FXML
    private TableColumn OctTemp;

    @FXML
    private TableView Nov;
    @FXML
    private TableColumn NovDay;
    @FXML
    private TableColumn NovTemp;

    @FXML
    private TableView Dec;
    @FXML
    private TableColumn DecDay;
    @FXML
    private TableColumn DecTemp;

    @FXML
    TextField textField;
    @FXML
    Button openbtn;
    @FXML
    Button dbtn;
    @FXML
    Button graphicBtn;

    @FXML
    private void OpenFile() {
        FileChooser fileChooser = new FileChooser();//Класс работы с диалогом выборки и сохранения
        fileChooser.setTitle("Open Document");//Заголовок диалога
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("СSV files (*.txt)", "*.txt");//Расширение
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showOpenDialog(null);//Указываем текущую сцену CodeNote.mainStage
        if (file != null) {
            textField.setText(file.toString());
        }
    }

    @FXML
    void DFile() {
        try (BufferedReader br = new BufferedReader(new FileReader(textField.getText()))) {
            String s;
            while ((s = br.readLine()) != null) {
                datas.add(new DateTemp(s.substring(0, 2),s.substring(3,5), s.substring(11)));
            }
        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
    }

    @FXML
    void Graphic() {
        lineChart.getData().removeAll(Collections.singleton(lineChart.getData().setAll()));

       // BarChart();
       // Table();
       // Table2();
        String boxValue = (String) Box.getValue();
        XYChart.Series series = new XYChart.Series();
        series.setName(boxValue);
        for (int i = 0; i < datas.size(); i++) {
            if (datas.get(i).getMonth().equals(boxValue))
                series.getData().add(new XYChart.Data(datas.get(i).getDay(), Integer.parseInt(datas.get(i).getTemp())));
        }
        lineChart.getData().add(series);
    }

    void BarChart() {
        XYChart.Series<String, Integer> dataSeries1 = new XYChart.Series<String, Integer>();
        dataSeries1.getData().add(new XYChart.Data<String, Integer>("Январь", Average( 1)));
        dataSeries1.getData().add(new XYChart.Data<String, Integer>("Февраль", Average(2)));
        dataSeries1.getData().add(new XYChart.Data<String, Integer>("Март", Average(3)));
        dataSeries1.getData().add(new XYChart.Data<String, Integer>("Апрель", Average(4)));
        dataSeries1.getData().add(new XYChart.Data<String, Integer>("Май", Average(5)));
        dataSeries1.getData().add(new XYChart.Data<String, Integer>("Июнь", Average(6)));
        dataSeries1.getData().add(new XYChart.Data<String, Integer>("Июль", Average(7)));
        dataSeries1.getData().add(new XYChart.Data<String, Integer>("Август", Average(8)));
        dataSeries1.getData().add(new XYChart.Data<String, Integer>("Сентябрь", Average(9)));
        dataSeries1.getData().add(new XYChart.Data<String, Integer>("Октябрь", Average(10)));
        dataSeries1.getData().add(new XYChart.Data<String, Integer>("Ноябрь", Average(11)));
        dataSeries1.getData().add(new XYChart.Data<String, Integer>("Декабрь", Average(12)));
        barChart.getData().add(dataSeries1);
    }

    int Average(Integer month) {
        int sum=0;
        int k=0;
for (int i=0;i<datas.size();i++){
        if(Integer.parseInt(datas.get(i).getMonth())==month)
    {
        sum+=Integer.parseInt(datas.get(i).getTemp());
        k++;
    }}
    sum=sum/k;
return sum;}

void Table()
{
    date.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("date"));
    temp.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("temp"));
    tableView.setItems(datas);
}
void  Table2()
{
    ObservableList<DateTemp> JanDatas = FXCollections.observableArrayList();
    ObservableList<DateTemp> FebDatas = FXCollections.observableArrayList();
    ObservableList<DateTemp> MarDatas = FXCollections.observableArrayList();
    ObservableList<DateTemp> AprDatas = FXCollections.observableArrayList();
    ObservableList<DateTemp> MayDatas = FXCollections.observableArrayList();
    ObservableList<DateTemp> JunDatas = FXCollections.observableArrayList();
    ObservableList<DateTemp> JulDatas = FXCollections.observableArrayList();
    ObservableList<DateTemp> AugDatas = FXCollections.observableArrayList();
    ObservableList<DateTemp> SepDatas = FXCollections.observableArrayList();
    ObservableList<DateTemp> OctDatas = FXCollections.observableArrayList();
    ObservableList<DateTemp> NovDatas = FXCollections.observableArrayList();
    ObservableList<DateTemp> DecDatas = FXCollections.observableArrayList();
    for (int i=0;i<datas.size();i++){
        if(Integer.parseInt(datas.get(i).getMonth())==1)
            JanDatas.add(datas.get(i));
        if(Integer.parseInt(datas.get(i).getMonth())==2)
            FebDatas.add(datas.get(i));
        if(Integer.parseInt(datas.get(i).getMonth())==3)
            MarDatas.add(datas.get(i));
        if(Integer.parseInt(datas.get(i).getMonth())==4)
            AprDatas.add(datas.get(i));
        if(Integer.parseInt(datas.get(i).getMonth())==5)
            MayDatas.add(datas.get(i));
        if(Integer.parseInt(datas.get(i).getMonth())==6)
            JunDatas.add(datas.get(i));
        if(Integer.parseInt(datas.get(i).getMonth())==7)
            JulDatas.add(datas.get(i));
        if(Integer.parseInt(datas.get(i).getMonth())==8)
            AugDatas.add(datas.get(i));
        if(Integer.parseInt(datas.get(i).getMonth())==9)
            SepDatas.add(datas.get(i));
        if(Integer.parseInt(datas.get(i).getMonth())==10)
            OctDatas.add(datas.get(i));
        if(Integer.parseInt(datas.get(i).getMonth())==11)
            NovDatas.add(datas.get(i));
        if(Integer.parseInt(datas.get(i).getMonth())==12)
            DecDatas.add(datas.get(i));
}
    JanDay.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("day"));
    JanTemp.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("temp"));
    Jan.setItems(JanDatas);

    FebDay.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("day"));
    FebTemp.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("temp"));
    Feb.setItems(FebDatas);

    MarDay.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("day"));
    MarTemp.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("temp"));
    Mar.setItems(MarDatas);

    AprDay.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("day"));
    AprTemp.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("temp"));
    Apr.setItems(AprDatas);

    MayDay.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("day"));
    MayTemp.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("temp"));
    May.setItems(MayDatas);

    JunDay.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("day"));
    JunTemp.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("temp"));
    Jun.setItems(JunDatas);

    JulDay.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("day"));
    JulTemp.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("temp"));
    Jul.setItems(JulDatas);

    AugDay.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("day"));
    AugTemp.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("temp"));
    Aug.setItems(AugDatas);

    SepDay.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("day"));
    SepTemp.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("temp"));
    Sep.setItems(SepDatas);

    OctDay.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("day"));
    OctTemp.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("temp"));
    Oct.setItems(OctDatas);

    NovDay.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("day"));
    NovTemp.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("temp"));
    Nov.setItems(NovDatas);

    DecDay.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("day"));
    DecTemp.setCellValueFactory (new PropertyValueFactory<DateTemp, String> ("temp"));
    Dec.setItems(DecDatas);
}}

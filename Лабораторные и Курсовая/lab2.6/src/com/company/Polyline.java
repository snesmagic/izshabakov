package com.company;

import java.util.ArrayList;
import java.util.Iterator;

public class Polyline implements Iterable<Point2D>
{
    ArrayList<Point2D> array;
    Polyline(ArrayList<Point2D> array)
    {
        this.array = array;
    }

    public double Length()
    {
        double S = 0;
        for (int i = 0; i < array.size() - 1; i++)
            S += Point2D.leng(array.get(i), array.get(i+1));
        return S;
    }
    public void  Add(Point2D point)
    {
        array.add(point);
    }

    @Override
    public Iterator<Point2D> iterator() {
        return array.iterator();
    }
}

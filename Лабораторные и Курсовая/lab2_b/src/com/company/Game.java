package com.company;

import java.util.Iterator;

public class Game implements Iterable<Integer>
{
    byte[][] array;
    Game(int n,int m)
{
    array = new byte[n][m];
}
    public void FirstGeneration()
    {
        for (int i = 0; i < array.length; i++)
            for (int l = 0; l < array[i].length;l++)
                array[i][l] = (byte) Math.round(Math.random());
    }
    public void NextGeneration()
    {
        byte[][]array1=new byte[array.length][array[1].length];
        for (int i = 0; i < array.length; i++)
            for (int l = 0; l < array[i].length;l++)
            {
                //Check(i, l);
                if (Check(i,l)==2 || Check(i,l)==3)
                    array1[i][l]=1;
                if (Check(i,l)<2 || Check(i,l)>3 )
                    array1[i][l]=0;

            }
        array=array1;
    }
    public int Check(int i, int l)
    {
        return IsLive(i-1,l-1)+IsLive(i-1,l)+IsLive(i-1,l+1)+IsLive(i,l+1)+IsLive(i+1,l+1)+IsLive(i+1,l)+IsLive(i+1,l-1)+IsLive(i,l-1);
    }
    public byte IsLive(int i,int l)
    {
        if (i==-1)
            i=array.length-1;
        if (i==array.length)
            i=0;
        if (l==-1)
            l=array[1].length-1;
        if (l==array[1].length)
            l=0;
        return array[i][l];
    }
    @Override
    public Iterator<Integer> iterator() {
        return null;
    }
    public String toString()
    {
        StringBuilder P = new StringBuilder();
        for (byte[] bytes : array)
        {
            P.append("|");
            for (byte aByte : bytes) {
                P.append(aByte).append("|");
            }
            P.append("\n");
        }
        return P.toString();
    }
}

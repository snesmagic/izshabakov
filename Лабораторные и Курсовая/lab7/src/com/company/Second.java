package com.company;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;


public class Second extends Application {
    public static void main(String[] args){
        Application.launch(args);
    }

    @Override public void start(Stage stage) {
        Label crd=new Label("Координаты точки на пл-ти");
        crd.setLayoutX(274);
        crd.setLayoutY(31);
        crd.prefHeight(17);
        crd.prefWidth(158);

        Label x=new Label("X:");
        x.setLayoutX(262);
        x.setLayoutY(58);

        Label y=new Label("Y:");
        y.setLayoutX(262);
        y.setLayoutY(93);

        Label answer=new Label();
        answer.setLayoutX(33);
        answer.setLayoutY(194);
        answer.prefHeight(25);
        answer.prefWidth(397);
        answer.setStyle("-fx-font-weight: bold");

        TextField X=new TextField();
        X.setLayoutX(279);
        X.setLayoutY(54);

        TextField Y=new TextField();
        Y.setLayoutX(279);
        Y.setLayoutY(89);

        Button check=new Button("Проверить");
        check.setLayoutX(309);
        check.setLayoutY(143);
        check.prefHeight(36);
        check.prefWidth(69);
        check.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                CheckFunc(X,Y);
                if((!X.getText().isEmpty())&& !Y.getText().isEmpty()){
                if(CheckFunc(X,Y)==true){
                    answer.setTextFill(Color.BLUE);
                    answer.setText("Точка"+ "("+X.getText()+";"+Y.getText()+")"+"принадлежит заданной области");}
                if(CheckFunc(X,Y)==false){
                    answer.setTextFill(Color.RED);
                    answer.setText("Точка"+ "("+X.getText()+";"+Y.getText()+")"+" не принадлежит заданной области");}
            }}

        });


        ImageView graphic=new ImageView(new Image(Second.class.getResourceAsStream("graphic.jpg")));
        graphic.setFitHeight(150);
        graphic.setFitWidth(200);
        graphic.setLayoutX(33);
        graphic.setLayoutY(14);
        graphic.setPickOnBounds(true);
        graphic.setPreserveRatio(true);

        AnchorPane anchorpane=new AnchorPane(crd,x,y,X,Y,answer,check,graphic);
        anchorpane.maxHeight(-1);
        anchorpane.maxWidth(-1);
        anchorpane.prefHeight(209);
        anchorpane.prefWidth(386);

        VBox vbox=new VBox(anchorpane);
        vbox.prefHeight(241);
        vbox.prefWidth(478);

        Scene scene = new Scene(vbox, 478, 241);
        stage.setTitle("Попадание в область");
        stage.setScene(scene);
        stage.getIcons().add(new Image(Third.class.getResourceAsStream("icon2.jpg")));
        stage.show();
    }
    public static boolean CheckFunc(TextField x,TextField y)
    {
try {
    double x1 = Double.parseDouble(x.getText());
    double y1 = Double.parseDouble(y.getText());
    double R = Math.sqrt(x1 * x1 + y1 * y1);
    double S = 2 * x1 + 6;
    //по-хорошему для случая (0:0) можно сделать отедельный If, но и так сойдет :)
    if (x1 >= 0 && (y1 > 0 || y1 <= 0)) {
        if ((R <= 6) && (x1 <= 3)) {
            return true;
        } else return false;
    }
    if (x1 < 0 && y1 >= 0) {
        if ((y1 <= S) && x1 >= -3 && y1 <= 6) {
            return true;
        } else return false;
    }
    //смысла указывать 3 четверть нет, так как остальные уеж указаны= автоматически выведется false


}catch (NumberFormatException e)
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Ошибка");
        alert.setHeaderText("Некорректный ввод");
        alert.show();
    }
return false;}}
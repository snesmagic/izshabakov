package com.company;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Third extends Application {
    ObservableList<String> words1 = FXCollections.observableArrayList("Кузнечик", "Вася", "Крокодил", "Орел","Трактор","Боинг 747","Слоник");
    ObservableList<String> words2 = FXCollections.observableArrayList("ушел", "полетел", "побежал", "залез", "уполз","упрыгал","умчался");
    ObservableList<String> words3 = FXCollections.observableArrayList("в кусты", "на дискотеку", "домой", "за пивом","на Чукотку","по грибы","на охоту");
    public static void main(String[] args){
        Application.launch(args);
    }

    public void start(Stage stage) {

        ComboBox<String> first=new ComboBox<>(words1);
        first.setValue(words1.get(0));
        Label lbll = new Label();
        lbll.setText(words1.get(0));
        first.setOnAction(event -> lbll.setText(first.getValue()));

        ComboBox<String> second=new ComboBox<>(words2);
        second.setValue(words2.get(0));
        Label lbl2 = new Label();
        lbl2.setText(words2.get(0));
        second.setOnAction(event -> lbl2.setText(second.getValue()));

        ComboBox<String> third=new ComboBox<>(words3);
        third.setValue(words3.get(0));
        Label lbl3 = new Label();
        lbl3.setText(words3.get(0));
        third.setOnAction(event -> lbl3.setText(third.getValue()));

        TextArea textArea=new TextArea();
        textArea.setMaxSize(300,1000);

        Button addBtn = new Button("Добавить фразу");
        Button closeBtn = new Button("Закрыть");
        Button clearBtn=new Button("Очистить");

        HBox combos= new HBox(first,second,third);
        VBox all=new VBox(combos,textArea,addBtn,clearBtn,closeBtn);

        Scene scene = new Scene(all, 400, 300);
        stage.setTitle("Электронный писатель");
        stage.setScene(scene);
        stage.getIcons().add(new Image(Third.class.getResourceAsStream("icon.png")));
        stage.show();

        addBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                textArea.setText(textArea.getText()+lbll.getText()+" "+lbl2.getText()+" "+lbl3.getText()+"\n");

            }
        });
        closeBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Platform.exit();
            }
        });
        clearBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                textArea.clear();

            }});
}
}

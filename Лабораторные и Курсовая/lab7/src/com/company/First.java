package com.company;
import javafx.application.Application;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.control.Label;

import java.util.ArrayList;

public class First extends Application {
static ArrayList<String> answers=new ArrayList<>(1);
static  double D;
    public static void main(String[] args) {
        Application.launch(args);
    }

    public void start(Stage stage) {

        Label label1=new Label("Введите коэффиценты:");
        label1.setAlignment(Pos.BASELINE_CENTER);
        label1.setMaxWidth (Double.MAX_VALUE);


        TextField a = new TextField();
        a.setMaxWidth(50);
        Text x2=new Text("x²+ ");
        TextField b = new TextField();
        b.setMaxWidth(50);
        Text x=new Text("x+ ");
        TextField c = new TextField();
        c.setMaxWidth(50);
        Text constant =new Text("=0");
        Text answer=new Text();
        Text disk=new Text();

        Button addBtn = new Button("Результат");
        HBox butttons=new HBox(addBtn);
        butttons.setAlignment(Pos.BASELINE_CENTER);
        HBox equation= new HBox(a,x2,b,x,c, constant);
        VBox all=new VBox(25,label1,equation,answer,disk,butttons);


        Scene scene = new Scene(all, 200, 200);
        stage.setTitle("Решение квадратного уравнения");
        stage.setScene(scene);

        stage.show();

        addBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                    Algo(a,b,c);
                    if(!answers.isEmpty()){
                        answer.setText("Корни:"+answers.toString());
                        disk.setText("D:"+ D);
                        answers.clear();}

            }});
}

        public static ArrayList Algo(TextField a, TextField b, TextField c) {
            try {
                double a1 = Double.parseDouble(a.getText());

                double b1 = Double.parseDouble(b.getText());

                double c1 = Double.parseDouble(c.getText());

                D = b1 * b1 - 4 * a1 * c1;
                if (D == 0) {
                    answers.add(String.valueOf((-b1 + Math.sqrt(D)) / (2 * a1)));
                }
                if (D > 0) {
                    answers.add(String.valueOf((-b1 + Math.sqrt(D)) / (2 * a1)));
                    answers.add(String.valueOf((-b1 - Math.sqrt(D)) / (2 * a1)));
                }
                if (D < 0) {
                    String first = String.format("%.2f", -b1 / (2 * a1));
                    String second = String.format("%.2f", Math.sqrt(-D) / (2 * a1));
                    answers.add(first + "±i" + second);
                }
            } catch (NumberFormatException e) {
                Label errorlabel = new Label("Пустая строка!");
                Button errorbutton = new Button("Ok");

                Stage errorstage = new Stage();
                errorstage.setWidth(150);
                errorstage.setHeight(100);
                errorstage.setTitle("Ошибка");

                HBox errorhbox=new HBox(errorlabel,errorbutton);
                Scene errorscene=new Scene(errorhbox,100,100);
                errorstage.setScene(errorscene);

                errorstage.show();

                errorbutton.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent actionEvent) {
                        errorstage.close();
                    }});
            }
            return answers;
        }}

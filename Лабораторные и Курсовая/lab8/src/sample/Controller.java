package sample;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import java.util.Random;
import static java.lang.Math.*;
import static javafx.scene.paint.Color.color;

public class Controller {
    @FXML
    private VBox parent;

    @FXML
    private Canvas canvas;
    @FXML
    private GraphicsContext context;
    @FXML
    private Button btn;
    @FXML
    private void click(){
        Random rand = new Random();
        float r = rand.nextFloat();
        float g = rand.nextFloat();
        float b = rand.nextFloat();
        int figures=(int) (random()*7);
        context = canvas.getGraphicsContext2D();

if (figures==0) {
    context.fillOval(random() * 100, random() * 100, random() * 100, random() * 100);

}
else if(figures==1){
    context.setStroke(color(r, g, b, 0.5)); // устанавливаем цвет
    context.strokeOval(Math.random() * 100, Math.random() * 100, Math.random() * 200, Math.random() * 200);
}
else if (figures==2) {
    context.setFill(color(r, g, b, 0.5)); // устанавливаем цвет
    context.fillRect(Math.random() * 100, Math.random() * 100, Math.random() * 200, Math.random() * 200);
} else if (figures==3) {
    context.setStroke(color(r, g, b, 0.5)); // устанавливаем цвет
    context.strokeRect(Math.random() * 100, Math.random() * 100, Math.random() * 200, Math.random() * 200);
} else if (figures==4) {
    context.setFill(color(r, g, b, 0.5));
    context.fillPolygon(
            new double[]{Math.random() * 100, Math.random() * 100, Math.random() * 100}, // X координаты вершин
            new double[]{Math.random() * 100, Math.random() * 100, Math.random() * 100}, // Y координаты вершин
            3 // количество вершин
    );
} else if (figures==5) {
    context.setStroke(color(r, g, b, 0.5));
    context.strokePolygon(
            new double[]{Math.random() * 100, Math.random() * 100, Math.random() * 100}, // X координаты вершин
            new double[]{Math.random() * 100, Math.random() * 100, Math.random() * 100}, // Y координаты вершин
            3 // количество вершин
    );
} else if (figures==6) {
    context.setStroke(color(r, g, b, 0.5)); // устанавливаем цвет
    context.strokeLine(Math.random() * 200, Math.random() * 100, Math.random() * 100, Math.random() * 100); // рисуем прямоугльник 60x90px с левым верним углом в точке (200; 50)
}
}
}

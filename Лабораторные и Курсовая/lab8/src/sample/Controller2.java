package sample;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import java.util.Random;
import static javafx.scene.paint.Color.color;

public class Controller2 {
    Random rand = new Random();
    float r = rand.nextFloat();
    float g = rand.nextFloat();
    float b = rand.nextFloat();

    @FXML
    private AnchorPane lineAncher,rectAncher,ellipseAncher,triangleAncher;

    @FXML
    private Canvas lineCanvas,rectCanvas,ellipseCanvas,triangleCanvas;

    @FXML
    private Button lineButton,rectButton,ellipseButton,triangleButton;

    //Координаты прямой
    @FXML
    private TextField lineX1,lineY1,lineX2,lineY2;

    //Координаты прямоугольника
    @FXML
    private TextField rectX1,rectY1,rectX2,rectY2;

    //Координаты эллипса
    @FXML
    private TextField ellipseX1,ellipseY1,ellipseX2,ellipseY2;

    //Координаты треугольника
    @FXML
    private TextField triangleX1,triangleY1,triangleX2,triangleY2,triangleX3,triangleY3;

    private GraphicsContext context;

    @FXML
    void lineClick()
    {
        try {
        context = lineCanvas.getGraphicsContext2D();
        int X1 = Integer.parseInt(lineX1.getText());
        int Y1 = Integer.parseInt(lineY1.getText());
        int X2 = Integer.parseInt(lineX2.getText());
        int Y2 = Integer.parseInt(lineY2.getText());

        context.setStroke(color(r,g,b,0.5));
        context.strokeLine(X1, Y1, X2, Y2);}
        catch (NumberFormatException e)
        {
            Alert error=new Alert(Alert.AlertType.ERROR);
            error.setHeaderText("Ошибка");
            error.setContentText("Неккоректный ввод");
            error.show();
        }
    }
    @FXML
    void rectClick()
    {
        try {
        context = rectCanvas.getGraphicsContext2D(); // и получаем GraphicContext
        int X1 = Integer.parseInt(rectX1.getText());
        int X2 = Integer.parseInt(rectY1.getText());
        int Widht= Integer.parseInt(rectX2.getText());
        int Height = Integer.parseInt(rectY2.getText());
        context.setFill(color(r,g,b,0.5)); // устанавливаем цвет
        context.fillRect(X1,X2,Widht,Height);}
        catch (NumberFormatException e)
        {
            Alert error=new Alert(Alert.AlertType.ERROR);
            error.setHeaderText("Ошибка");
            error.setContentText("Неккоректный ввод");
            error.show();
        }
    }
    @FXML
    void ellipseClick()
    {
        try {
        context = ellipseCanvas.getGraphicsContext2D();
        int X = Integer.parseInt(ellipseX1.getText());
        int X1 = Integer.parseInt(ellipseY1.getText());
        int Widht= Integer.parseInt(ellipseX2.getText());
        int Height = Integer.parseInt(ellipseY2.getText());
        context.setFill(color(r,g,b,0.5));
        context.fillOval(X,X1,Widht,Height);
    }catch (NumberFormatException e)
        {
            Alert error=new Alert(Alert.AlertType.ERROR);
            error.setContentText("Неккоректный ввод");
            error.setTitle("Ошибка");
            error.show();
        }
    }
    @FXML
    void triangleClick()
    {
        try {
        context = triangleCanvas.getGraphicsContext2D(); // и получаем GraphicContext
        int X1 = Integer.parseInt(  triangleX1.getText());
        int X2 = Integer.parseInt(triangleX2.getText());
        int X3 = Integer.parseInt(triangleX3.getText());
        int Y1 = Integer.parseInt(triangleY1.getText());
        int Y2 = Integer.parseInt(triangleY2.getText());
        int Y3 = Integer.parseInt(triangleY3.getText());
        context.setFill(color(r,g,b,0.5));
        context.fillPolygon(
                new double[]{X1, X2, X3}, // X координаты вершин
                new double[]{Y1, Y2, Y3}, // Y координаты вершин
                3 // количество вершин
        );}catch (NumberFormatException e){
            Alert error=new Alert(Alert.AlertType.ERROR);
            error.setContentText("Неккоректный ввод");
            error.setTitle("Ошибка");
            error.show();
        }
    }
}

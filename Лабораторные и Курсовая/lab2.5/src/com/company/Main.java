package com.company;

import java.io.*;
import java.util.*;

public class Main {
    private static ArrayList<String> arraylist = new ArrayList<>(5);
    private static final String separator = " ";

    public static void main(String[] args) {
        ReadCSV();

        arraylist.sort(Comparator.comparingInt(String::length));
        arraylist.sort((s, anotherString) -> s.compareTo(anotherString));
        arraylist.sort(Comparator.comparingInt(s ->
        {
            int count = 0;
            for (int i = 0; i < s.length(); i++)
                if (s.charAt(i) >= 'A' && s.charAt(i) <= 'Z')
                    count++;
            return -count;
        }));

        for (String i : arraylist) {
            System.out.println(" " + i);
        }
    }

    public static void ReadCSV() {
        String fname = "./src/com/company/txt.txt";
        try (BufferedReader br =
                     new BufferedReader(new InputStreamReader(new FileInputStream(fname)))) {
            String line = "";
            line = br.readLine();
            String[] elements = line.split(separator);
            arraylist.addAll(Arrays.asList(elements));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
